<?php

namespace TodoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Importance
 *
 * @ORM\Table(name="importance")
 * @ORM\Entity(repositoryClass="TodoBundle\Repository\ImportanceRepository")
 */
class Importance
{
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->todo = new \Doctrine\Common\Collections\ArrayCollection();
    }

    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="TodoBundle\Entity\Todo", mappedBy="importance")
     */
    private $todo;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Importance
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add todo
     *
     * @param \TodoBundle\Entity\Todo $todo
     *
     * @return Importance
     */
    public function addTodo(\TodoBundle\Entity\Todo $todo)
    {
        $this->todo[] = $todo;

        return $this;
    }

    /**
     * Remove todo
     *
     * @param \TodoBundle\Entity\Todo $todo
     */
    public function removeTodo(\TodoBundle\Entity\Todo $todo)
    {
        $this->todo->removeElement($todo);
    }

    /**
     * Get todo
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTodo()
    {
        return $this->todo;
    }
}
