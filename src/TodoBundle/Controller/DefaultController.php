<?php

namespace TodoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use TodoBundle\Entity\Todo;
use TodoBundle\Entity\Importance;
use TodoBundle\Form\TodoType;

class DefaultController extends Controller
{
    public function indexAction()
    {
    	$tasks = $this->getDoctrine()->getManager()->getRepository('TodoBundle:Todo')->MyFindAll();

    	//die(var_dump($tasks));
        return $this->render('TodoBundle:Default:index.html.twig', array( "tasks" => $tasks ));
    }


    public function showAction($id)
    {
    	
    	$task = $this->getDoctrine()->getManager()->getRepository('TodoBundle:Todo')->MyFindBy($id);

        if (!$task) {
            throw $this->createNotFoundException('Unable to find this task.');
        }

        return $this->render('TodoBundle:Default:show.html.twig', array( "task" => $task ));
    }


    Public function AddAction(Request $request, $id = 0)
	{
		$task = new Todo();

	 	// Edit action
		if($id > 0){
			$task = $this->getDoctrine()->getManager()->getRepository("TodoBundle:Todo")->MyFindBy($id);

		}

		// Création form
	  	$formBuilder = $this->get('form.factory')->createBuilder(new TodoType(), $task);
	  

	 	$form = $formBuilder->getForm();
	 	$form->add('submit', 'submit', array('attr' => array('class' => 'btn btn-success'), 'label' => 'Ajouter'));

	 	// Enregistrement data
	 	$form->handleRequest($request);
 
 		if($form->isSubmitted() && $form->isValid()) 
 		{
	 		$em = $this->getDoctrine()->getManager();

	      	$em->persist($task);

	      	$em->flush();
			$session = $request -> getSession();
			$session -> getFlashBag() -> add("info", "Opération validée : la tâche a bien été ajoutée !");
	      

	      	return  $this->redirect($this->generateUrl('todo_homepage'));
	 	}



	 	return $this->render('TodoBundle:Default:add.html.twig', array(
	            'form' => $form->createView(),
	        ));

	}




	Public function DeleteAction(Request $request, $id)
    {

        $em = $this->getDoctrine()->getManager();
        $task = $this->getDoctrine()->getManager()->getRepository("TodoBundle:Todo")->find($id);

        $em->remove($task);
        $em->flush();
        $session = $request -> getSession();
        $session -> getFlashBag() -> add("info", "Opération validée : la tâche a bien été supprimée  !");

        return  $this->redirect($this->generateUrl('todo_homepage'));
    }
}
