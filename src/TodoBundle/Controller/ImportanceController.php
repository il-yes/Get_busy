<?php

namespace TodoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use TodoBundle\Entity\Importance;

class ImportanceController extends Controller
{
    public function indexAction()
    {
    	$entities = $this->getDoctrine()->getManager()->getRepository('TodoBundle:Importance')->findAll();

    	//die(var_dump($entities));
        return $this->render('TodoBundle:Importance:all.html.twig', array( "entities" => $entities ));
    }


    public function showAction($id)
    {
    	
    	$entity = $this->getDoctrine()->getManager()->getRepository('TodoBundle:Importance')->find($id);
    	//die(var_dump($entities));
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find this entity.');
        }

        return $this->render('TodoBundle:Importance:show.html.twig', array( "entity" => $entity ));
    }
}
